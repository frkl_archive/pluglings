# -*- coding: utf-8 -*-

from plugtings.core import Plugting, EntryPointIndex, load_plugting


class PlugtingTings(Plugting):
    def __init__(self):

        index = EntryPointIndex(entry_point="plugtings")
        super(PlugtingTings, self).__init__(index=index)

        self._sub_plugtings = {}

    def get_default_plugin(self) -> str:
        return None

    def _get_sub_plugling(self, entry_point) -> Plugting:

        if entry_point not in self.plugins.keys():
            plugting = load_plugting(entry_point)
            self._sub_plugtings[entry_point] = plugting
        else:
            if entry_point not in self._sub_plugtings.keys():
                self._sub_plugtings[entry_point] = self._sub_plugtings[
                    entry_point
                ] = self.create_obj(entry_point)

        return self._sub_plugtings[entry_point]

    def create_object_from_plugin(self, plugin_type, entry_point, init_data):

        return self._get_sub_plugling(plugin_type).create_obj(
            entry_point=entry_point, init_data=init_data
        )
