# -*- coding: utf-8 -*-
import click

from frtls.cli.logging import logzero_option
from plugtings.core import Plugting, load_plugting, load_entry_point_plugting


@click.group()
@logzero_option()
@click.pass_context
def cli(ctx):

    pass


@cli.command()
@click.pass_context
def list_plugtings(ctx):

    from plugtings.core import load_registered_plugting_types

    pt = load_registered_plugting_types()
    for k, v in pt.items():
        print(f"{k}: {v}")


@cli.command()
@click.argument("plugting_alias", nargs=1, required=True)
@click.pass_context
def list_entry_points(ctx, plugting_alias):

    pt = load_entry_point_plugting(plugting_alias)

    for k, v in pt.plugins.items():
        print(f"{k}: {v}")


@cli.command()
@click.argument("base_class", nargs=1, required=True)
@click.pass_context
def list_subclasses(ctx, base_class):

    pt = Plugting(base_class=base_class, ensure_loaded=["tings.properties.core"])

    for k, v in pt.plugins.items():
        print(f"{k}: {v}")


@cli.command()
@click.argument("plugting_type_or_entry_point")
@click.argument("plugin")
@click.pass_context
def create_obj(ctx, plugting_type_or_entry_point, plugin):

    pt = load_plugting(plugting_type_or_entry_point)
    print(pt.create_obj(plugin))


if __name__ == "__main__":
    cli()
