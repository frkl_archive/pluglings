# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs

plugtings_app_dirs = AppDirs("plugtings", "frkl")

if not hasattr(sys, "frozen"):
    PLUGTINGS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `plugtings` module."""
else:
    PLUGTINGS_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "plugtings")
    """Marker to indicate the base folder for the `plugtings` module."""

PLUGTINGS_RESOURCES_FOLDER = os.path.join(PLUGTINGS_MODULE_BASE_FOLDER, "resources")
